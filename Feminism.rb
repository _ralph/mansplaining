#!/usr/bin/env ruby

require 'ruby-progressbar'
require 'pp'
require 'optparse'
require 'indentation'

@current_slide = 0

def start()
  puts 'For a list of commands, type -halp'
  actions()
end

def actions()
  prompt; input = gets.chomp
  firstWord  = input.split(' ').first
  secondWord = input.split(' ').last

  if input == "brew install feminism 4.0.1".downcase
    progressive()
  else
  end

  case firstWord
  when "-halp".downcase 
    halp
  when "slides".downcase
    slides
  when "resume".downcase
    slides
  when "advice".downcase
    advice
  when "ugh".downcase  
    ugh
  when "conf".downcase
    conf
  when "about".downcase
    about
  when "learn".downcase
    learn(secondWord)
  when "quit".downcase
    exit
  else
    actions()
  end
end

def prompt()
  print "> "
end

def progressive()
  puts "Now installing Feminism 4.0.1"
  progressbar = ProgressBar.create( :format         => '%a %bᗧ%i %p%% %t',
                    :progress_mark  => ' ',
                    :remainder_mark => '･',
                    :starting_at    => 10)
  90.times do progressbar.increment
  sleep 0.07
  end
  progressbar.log 'Successfully installed Feminism 4.0.1'
end

def halp()
  puts "About                 Learn about the project"
  puts "Advice                Randomized advice from women in tech, primarily for other women in tech."
  puts "Conf                  Randomized microaggressions women experienced in conferences/work parties/tech meetups"
  puts "Ugh                   Randomized microaggressions women experienced in the tech industry."
  puts "Learn                 List of external resources on the intersection of feminism and tech"
  puts "Slides                To view the slideshow, enter for next slide, 'back' to return a slide, 'break' to exit slideshow"
  puts "Exit                  This is only the beginning"
  actions()
end  

def about()
  about = File.open("data/about.txt", "r")
  aboutArray = about.readlines
  puts aboutArray
  actions()
end

def advice()
  adviceArray=[]
  advice = File.open("data/experiences/advice.txt", "r")
  adviceArray = advice.readlines.map{|line| line.strip}.reject do |line|
    line.length < 1
  end
  puts "\"" + adviceArray.sample + "\""
  actions()
end

def conf()
  confArray=[]
  conf = File.open("data/experiences/conference.txt", "r")
  confArray = conf.readlines.map{|line| line.strip}.reject do |line|
    line.length < 1
  end
  puts "\"" + confArray.sample + "\""
  actions()
end

def ugh()
  ughArray=[]
  ugh = File.open("data/experiences/microaggressions.txt", "r")
  ughArray = ugh.readlines.map{|line| line.strip}.reject do |line|
    line.length < 1
  end
  puts "\"" + ughArray.sample + "\""
  actions()
end

def learn(learned)
  if learned == "learn".downcase
    puts "Picking a resource will open a site in your default browser."
    puts "The types of resources are: Audio, Conference, Sites."
    prompt()
    learned = gets.chomp
  end  

  learned = learned.split(' ')
  learnArray=[]
  if learned.first == "resume"
    slides()
  else
    learn = File.open("data/information/resources/#{learned.last}.txt", "r")
    learnArray = learn.readlines.map{|line| line.strip}.reject do |line|
    line.length < 1
  end
  `open #{learnArray.sample}`
  end
end

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: feminism [options]"
  opts.on("-h", "--halp", "List commands") do
    halp()
    exit
  end
end.parse!

def slides()
  while @current_slide < 28
    system "clear"
    slide = File.open("data/slides/#{@current_slide}.txt", "r")
    if @current_slide == 1
      progressive()
    end
    if @current_slide != 3
      puts ""
      puts ""
      puts ""
    else
    end
    puts slide.read.indent(10)
    prompt()
    next_slide = gets.chomp

    case next_slide
    when "back"
      @current_slide -= 1 if @current_slide > 0
    when "break"
     start()
    else
      @current_slide += 1
    end
  end
end

start()
