This project was originally created by Marni Cohen for a talk at VelocityConf 2014 Europe called 'Mansplaining 101: Cisadmin Edition'

ABOUT THE PROJECT
The purpose of this is to provide resources about feminism.  It isn't the end-all-source, it doesn't automatically make you a good person for downloading, it's just another tool for learning.

The commands 'conf', 'ugh', and 'advice' all pull random quotes from a questionaire.  The questionaire was aimed at women and gender minorities in the tech industry, to provide an outlet for their stories.  By the time the survey was done, I had recieved more stories than I had time to share in my talk, so I created this program to help share experiences.  Seventy-five women and gender minorities responded to the survey with a broad range of experiences.  If you have a story you want to contribute to the project, add it to the bottom of the relevant data/information/resources*.txt file.

This project is free to add/use/edit/mess with! https://gitlab.com/marni/feminism
Questions/concerns/hate mail/love mail/feminist theory manifestos: marni@puppetlabs.com
